import {ADD_LIKE} from "../actions/types";

const initialState = {
    liked: false,
    likeStyle: {backgroundPosition: '1px -1px'},
    likes: 665
};

function likesReducer(state = initialState, action) {
    switch (action.type) {
        case ADD_LIKE:
            if (!state.liked) {
                return {...state, liked: true, likes: state.likes + 1, likeStyle: {backgroundPosition: '-25px -1px'}};
            } else {
                return {...state, liked: false, likes: state.likes - 1, likeStyle: {backgroundPosition: '1px -1px'}};
            }
        default:
            return state;
    }
}

export default likesReducer;