import React, {Component} from 'react';
import '../../styles/css/App.css'
import Post from "./Post";

class PostList extends Component {

    render() {
        return (
            <div className="postlist">
                <Post />
            </div>
        );
    }
}

export default PostList;