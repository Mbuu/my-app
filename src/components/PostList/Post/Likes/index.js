import React, {Component} from 'react';
import '../../../../styles/css/App.css'
import {ADD_LIKE} from "../../../../actions/types";
import {connect} from "react-redux";


class Likes extends Component {

    render() {

        const {addLike, likes} = this.props;

        return (
                <div className="like">
                    <div className="like__icon-wrapper" style={likes.likeStyle}
                         onClick={() => addLike()}/>
                    <a href="" className="like__number">{likes.likes + ' likes'}</a>
                </div>
        );
    }
}

const mapStoreToProps = (store) => {
    return {
        likes: store.likes
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        addLike: () => dispatch ({
            type: ADD_LIKE
        }),
    }
};

export default connect(mapStoreToProps, mapDispatchToProps)(Likes);