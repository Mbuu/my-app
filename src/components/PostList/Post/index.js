import React, {Component} from 'react';
import '../../../styles/css/App.css'
import photo from './post-photo.jpg'
import avatar from './avatar.jpg'
import Likes from "./Likes";
import Bookmarks from "./Bookmarks";
import Menu from "./Menu";

class Post extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bookmarked: false,
            bookmarkStyle: {backgroundPosition: '1px -2px'},
        };
    }

    render() {
        const {bookmarkStyle} = this.state;

        return (
            <div className="post">
                <div className="post__user">
                    <div className="post__avatar-wrapper">
                        <a href="" className="post__avatar-link">
                            <img src={avatar} alt="" className="post__avatar"/>
                        </a>
                    </div>
                    <div className="post__login-wrapper">
                        <a href="" className="post__login-link">nasty1990</a>
                    </div>
                </div>
                <img src={photo} alt="" className="post__photo"/>
                <div className="post__bilge">
                    <div className="post__likes-bookmarks-wrapper">
                        <Likes/>
                        <Bookmarks bookmarkStyle={bookmarkStyle} addToBookmarks={this.addToBookmarks}/>
                    </div>

                    <div className="post__temporary-wrapper">
                        <time className="post__date">2 DAYS AGO</time>
                        <Menu/>
                    </div>
                </div>
            </div>
        );
    }

    addToBookmarks = () => {
        if (!this.state.bookmarked) {
            this.setState({
                bookmarked: true,
                bookmarkStyle: {backgroundPosition: '-29px -2px'}
            })
        } else {
            this.setState({
                bookmarked: false,
                bookmarkStyle: {backgroundPosition: '1px -2px'}
            })
        }
    }
}

export default Post;