import React, {Component} from 'react';
import '../../../../styles/css/App.css'

class Bookmarks extends Component {

    render() {

        const {bookmarkStyle, addToBookmarks} = this.props;

        return (
            <div className="bookmark">
                <div className="bookmark__icon-wrapper" style={bookmarkStyle}
                     onClick={() => addToBookmarks()}/>
            </div>
        );
    }
}

export default Bookmarks;