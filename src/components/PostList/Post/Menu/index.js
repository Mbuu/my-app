import React, {Component, Fragment} from 'react';
import '../../../../styles/css/App.css'
import dots from './dots.svg'

class Menu extends Component {
    /*constructor(props) {
        super(props);
        this.state = {
            modalDisplay: {display: 'none'}
        };
    }*/

    render() {
       // const {modalDisplay} = this.state;

        return (
            <Fragment>
                <button className="button" onClick={() => this.componentDidMount()}><img src={dots} alt=""/></button>
                <div className="modal" onClick={() => this.componentDidMount()}>
                    <div className="modal__content">
                        <button className="modal__button">Go to post</button>
                        <button className="modal__button">Report inappropriate</button>
                        <button className="modal__button">Embed</button>
                        <button className="modal__button">Unfollow</button>
                        <button className="modal__button">Cancel</button>
                    </div>
                </div>
            </Fragment>
        );
    }

 /*   showModal() {
        this.setState({
            modalDisplay: this.state.modalDisplay = {display: 'flex'}
        })
    }*/

    componentDidMount () {
        let modal = document.querySelector('.modal');
        let button = document.querySelector('.button');

        button.onclick = function (){
            modal.style.display = "flex";
        };

        window.onclick = function(event) {
            if (event.target === modal) {
                modal.style.display = "none";
            }
        }
    }
}

export default Menu;