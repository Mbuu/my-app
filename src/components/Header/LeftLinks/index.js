import React, {Component} from 'react';
import '../../../styles/css/App.css';
import insta_logo from './instagram-icon.svg';
import photo_logo from './photo-icon.svg';
import {Link} from "react-router-dom";


class LeftLinks extends Component {
    render() {
        return (
            <div className="left-links">
                <Link to="/" className="left-links__link left-links__link--photo">
                    <img className="left-links__logo" src={photo_logo} alt=""/>
                </Link>
                <div className="left-links__vertical-line"/>
                <Link to="/" className="left-links__link left-links__link--insta">
                    <img className="left-links__logo" src={insta_logo} alt=""/>
                </Link>
            </div>
        )
    }
}

export default LeftLinks;