import React, {Component} from 'react';
import '../../../styles/css/App.css';


class Search extends Component {
    render() {
        return (
            <form action="" className="search">
                <input type="text" id="search" className="search__field" placeholder="search"/>
                <label htmlFor="search" className="search__icon"><i className="fa fa-search"/></label>
            </form>
        )
    }
}

export default Search;