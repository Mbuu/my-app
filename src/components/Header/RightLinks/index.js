import React, {Component} from 'react';
import '../../../styles/css/App.css';
import heart_icon from './heart.svg';
import compass_icon from './compass.svg';
import user_icon from './user.svg';
import {Link} from "react-router-dom";


class RightLinks extends Component {
    render() {
        return (
            <div className="right-links">
                <Link to="" className="right-links__link">
                    <img className="right-links__icon right-links__icon--compass" src={compass_icon} alt=""/>
                </Link>
                <Link to="" className="right-links__link">
                    <img className="right-links__icon" src={heart_icon} alt=""/>
                </Link>
                <Link to="/profile" className="right-links__link">
                    <img className="right-links__icon right-links__icon--user" src={user_icon} alt=""/>
                </Link>
            </div>
        )
    }
}

export default RightLinks;