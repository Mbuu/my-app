import React, {Component} from 'react';
import '../../styles/css/App.css'
import LeftLinks from "./LeftLinks";
import RightLinks from "./RightLinks";
import Search from "./Search";

class Header extends Component {

    render() {
        return (
            <header className="header">
                <div className="header__container">
                    <LeftLinks/>
                    <Search/>
                    <RightLinks/>
                </div>
            </header>
        );
    }
}

export default Header;