import React, {Component, Fragment} from 'react';

class NotFound extends Component {

    render() {
        return (
            <Fragment>
                <h1 className="error">404 ERROR</h1>
                <h4 className="not-found">Page Not Found</h4>
            </Fragment>
        );
    }
}

export default NotFound;