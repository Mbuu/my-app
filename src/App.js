import React, {Component, Fragment} from 'react';
import './styles/css/App.css';
import Header from './components/Header';
import PostList from "./components/PostList";
import {Route, Switch} from "react-router-dom";
import Profile from "./components/Profile";
import NotFound from "./components/NotFound";

class App extends Component {
    render() {
        return (
            <Fragment>
                <Header/>
                <Switch>
                    <Route path='/profile' component={Profile}/>
                    <Route exact path='/' component={PostList}/>
                    <Route path='*' component={NotFound}/>
                </Switch>
            </Fragment>
        );
    }
}

export default App;

